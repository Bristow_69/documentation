# Setup your account

## Create an account on PeerTube

To be able to upload a video, you must have an account on an _instance_ (a server running PeerTube). The PeerTube project maintains a list of
public instances at [instances.joinpeertube.org](https://instances.joinpeertube.org/instances) (note that instance administrators must add themselves
to this list manually, it is not automatically generated).

Once you've found an instance that suits you, click the button "Create an account" and fill in you **login** (nickname), a **email address** and a **password**.

![Registration page after clicking on "Create an account"](/assets/profile_registration.png)

## Connect to your PeerTube instance

To log in, you must go to the address of the particular instance that you registered on. Instances share each other's videos, but
each instance's index of accounts is not federated. Click on the "Login" button in the top-left corner, then fill in your login/email address and 
password. Note that the login name and password are **case-sensitive**!

Once logged in, your nickname and mail address will appear below the instance name.

## Update your profile

To update your user profile, change your avatar, change your password, etc. click on the three vertical dots near your profile name to pop
up a menu, then on the "My account" item. You then have a few options:

1.  My settings
1.  My library
1.  Ownership changes

![Account library management and settings](/assets/profile_library.png)

In the tab **_My Settings_**, you can:

* change your avatar by uploading a .png, .jpeg, .jpg, with a maximum size of 100KB from your computer.
* see your used/maximum upload quota. This quota is determined by the instance hosting your account, and can vary from a few MB to thousands of GB,
or even be unlimited. On instances that create different quality versions of your videos after uploading them (after a video transcoding step),
the disk space occupied by all versions of the video is taken into account, not just the one you uploaded.
* modify your display name, which is different from your login name. Your login name cannot be modified once created.
* add a user description, which will be displayed on your public profile. It is often the first thing seen by visitors and users of PeerTube, and represents your identity, so don't neglect it!
* change your password, by typing your password twice and clicking **_Change Password_**.
* on some instances, you can choose how to display videos with sensitive content: with blur, without blur or unlisted.
* you can choose to play videos automatically or not with a checkbox.
